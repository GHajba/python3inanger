__author__ = 'GHajba'

max_number = None
min_number = None

for s in input('Enter a list of numbers separated by spaces: ').split():
    try:
        n = int(s)
    except ValueError:
        continue
    if not min_number or min_number > n:
        min_number = n

    if not max_number or max_number < n:
        max_number = n

if min_number:
    print("The minimum of the list is {}, the maximum of the list is {}".format(min_number, max_number))
else:
    print("No valid numbers were entered.")
