__author__ = 'GHajba'

import random

scissors = 0
rock = 1
paper = 2

symbols = {'rock':1, 'paper':2, 'scissors':0}
number_to_symbol = {0:'scissors', 1:'rock', 2:'paper'}

while True:
    user_choice = input("Enter your choice (rock / paper / scissors): ")
    if user_choice.lower() not in symbols:
        print("Thank you for playing!")
        exit()
    computer_choice = random.randint(0,len(symbols)-1)
    print("Player chooses {}, computer chooses {}".format(user_choice, number_to_symbol[computer_choice]))
    user = symbols[user_choice]
    difference = user - computer_choice % len(symbols)
    if not difference:
        print("Tie!")
    elif difference > 0:
        print("User wins!")
    else:
        print("Computer wins!")
