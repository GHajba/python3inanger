__author__ = 'GHajba'

import random

while True:
    while True:
        try:
            max_number = int(input('Should the secret number between 1 and 100 or 1 and 1000? '))
        except ValueError:
            print("This was not a number!")
            continue
        if max_number != 100 and max_number != 1000:
            continue
        else:
            break

    if max_number == 100:
        guess_count = 7
    else:
        guess_count = 10


    print('You have chosen {}, you will have {} guesses to find the secret number.'.format(max_number, guess_count))

    secret_number = random.randint(1, max_number)

    print('I have chosen the secret number...')

    guesses = 0

    while guess_count - guesses:
        try:
            guesses += 1
            guessed = int(input("What's your guess? "))
        except ValueError:
            continue
        if guessed == secret_number:
            print('Congrats, you have Won!')
            break
        elif guessed > secret_number:
            print('The secret number is lower...')
        else:
            print('The secret number is higher...')
    else:
        print("Sorry, you lose.")

    print("The secret number was ", secret_number)
    answer = ''
    while answer.lower() not in ['yes', 'no', 'y', 'n']:
        answer = input("Do you want to play another round? (yes / no) ")

    if 'no' == answer or 'n' == answer:
        break
