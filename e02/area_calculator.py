input_confirm = "You entered the dimensions of {} meters by {} meters."
results = "The area of the shape is {} square-meters, which is {} square-feet."
conversion = 0.09290304

l = input('What is the length of the shape in meters? > ')
h = input('What is the height of the shape in meters? > ')

print(input_confirm.format(l,h))
square_meters = float(l)*float(h)
square_feet = round(square_meters/conversion, 3)
print(results.format(square_meters, square_feet))
