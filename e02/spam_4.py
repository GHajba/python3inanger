egg = 'egg'
sausage = 'sausage'
bacon = 'bacon'
spam = 'Spam'

print("The menu is:".capitalize())
print("{0} and {1}".format(egg, bacon).capitalize())
print("{0}, {1} and {2}".format(egg, sausage, bacon).capitalize())
print("{0} and {1}".format(egg, spam).capitalize())
print("{0}, {1} and {2}".format(egg, bacon, spam).capitalize())
print("{0}, {1}, {2} and {3}".format(egg, bacon, sausage, spam).capitalize())
print("{0}, {1}, {2} and {0}".format(spam, bacon, sausage).capitalize())
print("{0}, {1}, {0}, {0}, {2} and {0}".format(spam, egg, bacon).capitalize())
print("{0}, {0}, {0}, {1} and {0}".format(spam, egg).capitalize())
print("{0}, {0}, {0}, {0}, {0}, {0}, baked beans, {0}, {0}, {0} and {0}".format(spam).capitalize())
print("Lobster Thermidor aux crevettes with a Mornay sauce, garnished with truffle pâté, brandy and a fried {0} on top, and {1}.".format(egg, spam).capitalize())
