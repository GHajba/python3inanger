egg = 'egg'
sausage = 'sausage'
bacon = 'bacon'
spam = 'Spam'

print("The menu is:")
print("%s and %s"%(egg, bacon))
print("%s, %s and %s"%(egg, sausage, bacon))
print("%s and %s"%(egg, spam))
print("%s, %s and %s"%(egg, bacon, spam))
print("%s, %s, %s and %s"%(egg, bacon, sausage, spam))
print("%s, %s, %s and %s"%(spam, bacon, sausage, spam))
print("%s, %s, %s, %s, %s and %s"%(spam, egg, spam, spam, bacon, spam))
print("%s, %s, %s, %s and %s"%(spam, spam, spam, egg, spam))
print("%s, %s, %s, %s, %s, %s, baked beans, %s, %s, %s and %s"%(spam, spam, spam, spam, spam, spam, spam, spam, spam, spam))
print("Lobster Thermidor aux crevettes with a Mornay sauce, garnished with truffle pâté, brandy and a fried %s on top, and %s."%(egg, spam))
