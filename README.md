# Python 3 in Anger

This is the repository for code examples to my book [Python 3 in Anger](https://leanpub.com/python3inanger) published at LeanPub.

The structure of the repository is not final, I will improve it and make it more easy to use. This means I will try to align up the folder names with the chapters' numbers but it takes time because the order of the chapters is not final -- and I have exercise chapters which are numbered in the same order than other chapters.

Currently e01, e02, e03 and e04 contain example applications of the first four "Exercise" chapters. Perhaps it would be better to add the chapter name after the number to find the required scripts more easy.